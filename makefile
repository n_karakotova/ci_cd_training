CC := g++

CPPFLAGS := -std=c++20

OBJS := out/main.o
SRCS := main.cpp

build : app.exe

app.exe : $(OBJS)
	$(CC) -o $@ $^

out/%.d : %.cpp
	mkdir -p out
	$(CC) -M $< > $@

out/%.o : %.cpp
	$(CC) $(CPPFLAGS) -c $< -o $@

include $(SRCS:%.cpp=out/%.d)

.PHONY : clear

clear :
	rm -f out/*.o out/*.d *.exe
