#include <algorithm>
#include <array>
#include <iostream>

#include "circle.h"

int main(void)
{
    double x1, x2, x3, y1, y2, y3, r1, r2, r3;
    // scanf("Введите параметры первой окружности (x y r): %lf %lf %lf \n", &x1, &y1, &r1);
    // scanf("Введите параметры второй окружности (x y r): %lf %lf %lf \n", &x2, &y2, &r2);
    // scanf("Введите параметры третьей окружности (x y r): %lf %lf %lf \n", &x2, &y2, &r2);

    std::cin >> x1 >> y1 >> r1;
    std::cin >> x2 >> y2 >> r2;
    std::cin >> x3 >> y3 >> r3;

    std::array<Circle, 3> circles = {Circle(x1, y1, r1), Circle(x2, y2, r2), Circle(x3, y3, r3)};
 
    std::sort(circles.begin(), circles.end());
    std::cout << "Отсортировано:\n";

    for (auto c : circles)
        std::cout << "x y r: " << c.get_x() << " " << c.get_y() << " " << c.get_radius() << '\n';

    return 0;
}
