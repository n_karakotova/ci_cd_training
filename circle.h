#include <math.h>

class Circle
{
public:
    Circle() = default;

    Circle(const float &x, const float &y, const float &r) : x(x), y(y), r(r) {}

    double get_radius() const  { return r; }
    double get_x() const { return x; }
    double get_y() const { return y; }

    bool operator==(const Circle &c) const { return r == c.get_radius(); }
    bool operator!=(const Circle &c) const { return r != c.get_radius(); }

    bool operator>(const Circle &c) const { return r > c.get_radius(); }
    bool operator>=(const Circle &c) const { return r >= c.get_radius(); }

    bool operator<(const Circle &c) const { return r < c.get_radius(); }
    bool operator<=(const Circle &c) const { return r <= c.get_radius(); }

    double square() { return M_PI * r * r; }

    double perimeter() { return 2 * M_PI * r; }

    ~Circle() = default;

private:
    float x;
    float y;
    float r;
};
